# Contributing to the ABET accreditation platform

Thank you for your interest in contributing to the [ABET accreditation platform][ABET accreditation].
There are many ways to contribute, and we appreciate all of them :

- [Source code](#source-code)
- [Unit tests](#unit-tests)
- [Graphics](#graphics)
- [Bug Reports](#bug-reports)
- [Documentation](#writing-documentation-and-tutorials)
- [Localization (Translation)](#localization)

See [license terms](#contribution-license-terms) for contribution.

## Source code


## Unit tests


## Graphics


## Bug Reports


## Writing documentation and tutorials


## Localization


## Contribution license terms

Contributions should be under the terms of both the MIT license
[&lt;LICENSE-MIT&gt;][LICENSE-MIT] and the Apache License (Vertion 2.0)
[&lt;LICENSE-APACHE&gt;][LICENSE-APACHE].

Documentation and graphics contributions should be under the terms
of the MIT license [&lt;LICENSE-MIT&gt;][LICENSE-MIT], the Apache License
(Version 2.0) [&lt;LICENSE-APACHE&gt;][LICENSE-APACHE] and the Creative Commons
Attribution 4.0 International License [&lt;DOC-LICENSE&gt;][DOC-LICENSE].

The list of individuals and organizations who have contributed source code to this
project can be found in [AUTHORS][AUTHORS].

The list of individuals and organizations who have contibuted graphics, documentation,
tutorials, translations or other materials can be found in [CONTRIBUTORS][CONTRIBUTORS].



[AUTHORS]: AUTHORS
[CONTRIBUTORS]: CONTRIBUTORS
[COPYRIGHT]: COPYRIGHT
[ABET accreditation]: https://gitlab.com/GeeksFactory/abet-accreditation
[DOC-LICENSE]: DOC-LICENSE
[LICENSE-APACHE]: LICENSE-APACHE
[LICENSE-MIT]: LICENSE-MIT

