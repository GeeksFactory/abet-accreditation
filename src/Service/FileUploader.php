<?php
/**
 * This file is part of the ABET accreditation platform:
 * https://gitlab.com/GeeksFactory/abet-accreditation
 *
 * Copyright (C) 2019 Marwen Dallel <marwen.dallel@medtech.tn>
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License")
 * <LICENSE-APACHE> or the MIT license <LICENSE-MIT>, at your option.
 * You may not use this file except in compliance with one of the two licenses.
 * You should have received a copy of both the Apache License, Version 2.0 and
 * the MIT license along with this project. If not, you may obtain a copy of
 * the Apache License, Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * and you may obtain a copy of the MIT license at
 * https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, $year)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $originalFileExt = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $fileName = $safeFilename.'-'.$year.'.'.$originalFileExt;

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
