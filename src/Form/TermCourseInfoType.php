<?php
/**
 * This file is part of the ABET accreditation platform:
 * https://gitlab.com/GeeksFactory/abet-accreditation
 *
 * Copyright (C) 2019 Amine Ben Hassouna <amine.benhassouna@gmail.com>
 * Copyright (C) 2019 Marwen Dallel <marwen.dallel@medtech.tn>
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License")
 * <LICENSE-APACHE> or the MIT license <LICENSE-MIT>, at your option.
 * You may not use this file except in compliance with one of the two licenses.
 * You should have received a copy of both the Apache License, Version 2.0 and
 * the MIT license along with this project. If not, you may obtain a copy of
 * the Apache License, Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * and you may obtain a copy of the MIT license at
 * https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Form;

use App\Entity\User;
use App\Entity\TermCourseInfo;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TermCourseInfoType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
        // history (s), circle (r), check-circle (s)
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options['type'] === 'new') {
            $builder
                ->add('name')
                ->add('term')
                ->add('year')
            ;
        }

        $builder
            ->add('coordinator')
            ->add('teachers', null, [
                'by_reference' => false
            ])
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                [$this, 'hidePrivilegedFields']
            )
        ;

        if($options['type'] === 'edit') {
            $status = ['choices' => ['Pending' => 'PENDING', 'Validated' => 'Validated']];
            $builder
                ->add('syllabi', TextType::class, ['disabled' => true])
                ->add('syllabiFile', FileType::class, [
                    'label' => 'Upload syllabi',
                    'mapped' => false,
                    'required' => false
                ])
                ->add('syllabiStatus', ChoiceType::class, $status)

                ->add('artifacts', TextType::class, ['disabled' => true])
                ->add('artifactsFile', FileType::class, [
                    'label' => 'Upload Artifacts',
                    'mapped' => false,
                    'required' => false
                ])
                ->add('artifactsStatus', ChoiceType::class, $status)

                ->add('outcomes', TextType::class, ['disabled' => true])
                ->add('outcomesFile', FileType::class, [
                    'label' => 'Upload Outcomes',
                    'mapped' => false,
                    'required' => false
                ])
                ->add('outcomesStatus', ChoiceType::class, $status)

                ->add('representativeFirstName')
                ->add('representativeLastName')

                ->addEventListener(
                    FormEvents::PRE_SET_DATA,
                    [$this, 'hideStatus']
                )
            ;
        }
    }

    public function hidePrivilegedFields(FormEvent $event)
    {
        $form = $event->getForm();
        $user = $this->security->getUser();
        $course = $event->getData();

        if(!in_array('ROLE_ADMIN', $user->getRoles())) {
            $form->remove('coordinator')
                 ->remove('name')
                 ->remove('term')
                 ->remove('year');
            if($course->getCoordinator() !== $user) {
                $form->remove('teachers');
            }
        }

    }

    public function hideStatus(FormEvent $event)
    {
        $course = $event->getData();
        $form = $event->getForm();
        if ($course->getsyllabiStatus() === "EMPTY")
            $form->remove('syllabiStatus');
        if ($course->getArtifactsStatus() === "EMPTY")
            $form->remove('artifactsStatus');
        if ($course->getOutComesStatus() === "EMPTY")
            $form->remove('outcomesStatus');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TermCourseInfo::class,
            'type' => null
        ]);

        $resolver->setAllowedTypes('type', ['null', 'string']);
    }
}
