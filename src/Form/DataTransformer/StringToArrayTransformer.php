<?php
/**
 * This file is part of the ABET accreditation platform:
 * https://gitlab.com/GeeksFactory/abet-accreditation
 *
 * Copyright (C) 2019 Marwen Dallel <marwen.dallel@medtech.tn>
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License")
 * <LICENSE-APACHE> or the MIT license <LICENSE-MIT>, at your option.
 * You may not use this file except in compliance with one of the two licenses.
 * You should have received a copy of both the Apache License, Version 2.0 and
 * the MIT license along with this project. If not, you may obtain a copy of
 * the Apache License, Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * and you may obtain a copy of the MIT license at
 * https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StringToArrayTransformer implements DataTransformerInterface
{
    /**
     * Transforms an array to a string.
     * POSSIBLE LOSS OF DATA
     *
     * @return string
     */
    public function transform($array)
    {
        return $array[0];
    }

    /**
     * Transforms a string to an array.
     *
     * @param  string $string
     *
     * @return array
     */
    public function reverseTransform($string)
    {
        return array($string);
    }
}
