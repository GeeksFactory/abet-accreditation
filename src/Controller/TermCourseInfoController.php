<?php
/**
 * This file is part of the ABET accreditation platform:
 * https://gitlab.com/GeeksFactory/abet-accreditation
 *
 * Copyright (C) 2019 Amine Ben Hassouna <amine.benhassouna@gmail.com>
 * Copyright (C) 2019 Marwen Dallel <marwen.dallel@medtech.tn>
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License")
 * <LICENSE-APACHE> or the MIT license <LICENSE-MIT>, at your option.
 * You may not use this file except in compliance with one of the two licenses.
 * You should have received a copy of both the Apache License, Version 2.0 and
 * the MIT license along with this project. If not, you may obtain a copy of
 * the Apache License, Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * and you may obtain a copy of the MIT license at
 * https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controller;

use App\Entity\TermCourseInfo;
use App\Form\TermCourseInfoType;
use App\Service\FileUploader;
use App\Repository\TermCourseInfoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/term/course")
 */
class TermCourseInfoController extends AbstractController
{
    /**
     * @Route("/", name="term_course_index", methods={"GET"})
     */
    public function index(Request $request, TermCourseInfoRepository $termCourseInfoRepository): Response
    {
        $term = $request->query->get('term');
        $year = $request->query->get('year');
        if(!$term) $year = null;
        if(!$year) $term = null;

        if ($this->isGranted('ROLE_ADMIN')) {
            if(!$term) {
                $termCourseInfos = $termCourseInfoRepository->findBy([], ['year' => 'DESC', 'term' => 'ASC']);
            } else {
                $termCourseInfos = $termCourseInfoRepository->findByTermAndYear($term, $year);
            }
        } else {
            if(!$term) {
                $termCourseInfos = $termCourseInfoRepository->findByTeacher($this->getUser());
            } else {
                $termCourseInfos = $termCourseInfoRepository->findByTeacherAndTermAndYear($this->getUser(), $term, $year);
            }
        }

        return $this->render('term_course_info/index.html.twig', [
            'termCourseInfos' => $termCourseInfos,
            'term' => $term,
            'year' => $year,
        ]);
    }

    /**
     * @Route("/new", name="term_course_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $term = $request->query->get('term');
        $year = $request->query->get('year');
        if(!$term) $year = null;
        if(!$year) $term = null;

        $termCourseInfo = new TermCourseInfo();
        if($term) {
            $termCourseInfo->setTerm($term);
            $termCourseInfo->setYear($year);
        }
        $form = $this->createForm(TermCourseInfoType::class, $termCourseInfo, [
            'type' => 'new'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($termCourseInfo);
            $entityManager->flush();

            return $this->redirectToRoute('term_course_index');
        }

        return $this->render('term_course_info/new.html.twig', [
            'termCourseInfo' => $termCourseInfo,
            'form' => $form->createView(),
            'term' => $term,
            'year' => $year,
        ]);
    }

    /**
     * @Route("/{id}", name="term_course_show", methods={"GET"})
     */
    public function show(TermCourseInfo $termCourseInfo): Response
    {
        return $this->render('term_course_info/show.html.twig', [
            'termCourseInfo' => $termCourseInfo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="term_course_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TermCourseInfo $termCourseInfo, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(TermCourseInfoType::class, $termCourseInfo, [
            'type' => 'edit'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $syllabiFile */
            $syllabiFile = $form['syllabiFile']->getData();
            /** @var UploadedFile $artifactsFile */
            $artifactsFile = $form['artifactsFile']->getData();
            /** @var UploadedFile $outcomesFile */
            $outcomesFile = $form['outcomesFile']->getData();

            if ($syllabiFile) {
                $syllabiFileName = $fileUploader->upload($syllabiFile, $termCourseInfo->getYear());
                $termCourseInfo->setSyllabi($syllabiFileName);
                $termCourseInfo->setSyllabiStatus('PENDING');
            }
            if ($artifactsFile) {
                $artifactsFileName = $fileUploader->upload($artifactsFile, $termCourseInfo->getYear());
                $termCourseInfo->setArtifacts($artifactsFileName);
                $termCourseInfo->setArtifactsStatus('PENDING');
            }
            if ($outcomesFile) {
                $outcomesFileName = $fileUploader->upload($outcomesFile, $termCourseInfo->getYear());
                $termCourseInfo->setOutcomes($outcomesFileName);
                $termCourseInfo->setOutcomesStatus('PENDING');
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('term_course_index', [
                'id' => $termCourseInfo->getId(),
            ]);
        }

        return $this->render('term_course_info/edit.html.twig', [
            'termCourseInfo' => $termCourseInfo,
            'form' => $form->createView(),
        ]);
    }

    private function setDocumentState(Request $request, TermCourseInfo $termCourseInfo, string $document, string $state): Response
    {
        $term = $request->query->get('term');
        $year = $request->query->get('year');
        if(!$term) $year = null;
        if(!$year) $term = null;

        if($document == 'syllabi') {
            $termCourseInfo->setSyllabiStatus($state);
        } else if($document == 'artifacts') {
            $termCourseInfo->setArtifactsStatus($state);
        } else if($document == 'outcomes') {
            $termCourseInfo->setOutcomesStatus($state);
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('term_course_index', [
            'term' => $term,
            'year' => $year,
        ]);
    }

    /**
     * @Route("/{id}/validate/{document}", name="term_course_validate", methods={"GET"})
     */
    public function validate(Request $request, TermCourseInfo $termCourseInfo, string $document): Response
    {
        return $this->setDocumentState($request, $termCourseInfo, $document, 'VALIDATED');
    }

    /**
     * @Route("/{id}/unvalidate/{document}", name="term_course_unvalidate", methods={"GET"})
     */
    public function unvalidate(Request $request, TermCourseInfo $termCourseInfo, string $document): Response
    {
        return $this->setDocumentState($request, $termCourseInfo, $document, 'PENDING');
    }

    /**
     * @Route("/{id}", name="term_course_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, TermCourseInfo $termCourseInfo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$termCourseInfo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($termCourseInfo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('term_course_index');
    }
}
