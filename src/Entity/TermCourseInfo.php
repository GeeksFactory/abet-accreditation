<?php
/**
 * This file is part of the ABET accreditation platform:
 * https://gitlab.com/GeeksFactory/abet-accreditation
 *
 * Copyright (C) 2019 Amine Ben Hassouna <amine.benhassouna@gmail.com>
 * Copyright (C) 2019 Marwen Dallel <marwen.dallel@medtech.tn>
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License")
 * <LICENSE-APACHE> or the MIT license <LICENSE-MIT>, at your option.
 * You may not use this file except in compliance with one of the two licenses.
 * You should have received a copy of both the Apache License, Version 2.0 and
 * the MIT license along with this project. If not, you may obtain a copy of
 * the Apache License, Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * and you may obtain a copy of the MIT license at
 * https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TermCourseInfoRepository")
 */
class TermCourseInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="terms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $term;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 2010,
     *      max = 2050,
     *      minMessage = "Invalid date",
     * )
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $representativeFirstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $representativeLastName;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $coordinator;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(
     *     maxSize = "5M",
     *     maxSizeMessage = "Please upload a valid PDF"
     * )
     */
    private $syllabi;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('EMPTY', 'PENDING', 'VALIDATED')")
     */
    private $syllabiStatus;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $syllabiComment;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $artifacts;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('EMPTY', 'PENDING', 'VALIDATED')")
     */
    private $artifactsStatus;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $artifactsComment;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $outcomes;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('EMPTY', 'PENDING', 'VALIDATED')")
     */
    private $outcomesStatus;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $outcomesComment;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="courses")
     * @ORM\JoinTable(name="user_courses")
     */
    private $teachers;

    public function __construct()
    {
        $this->teachers = new ArrayCollection();
        $this->year = date('Y');
        $this->syllabiStatus = 'EMPTY';
        $this->artifactsStatus = 'EMPTY';
        $this->outcomesStatus = 'EMPTY';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?Course
    {
        return $this->name;
    }

    public function setName(?Course $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTerm(): ?string
    {
        return $this->term;
    }

    public function setTerm(string $term): self
    {
        $this->term = $term;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getRepresentativeFirstName(): ?string
    {
        return $this->representativeFirstName;
    }

    public function setRepresentativeFirstName(?string $representativeFirstName): self
    {
        $this->representativeFirstName = $representativeFirstName;

        return $this;
    }

    public function getRepresentativeLastName(): ?string
    {
        return $this->representativeLastName;
    }

    public function setRepresentativeLastName(?string $representativeLastName): self
    {
        $this->representativeLastName = $representativeLastName;

        return $this;
    }

    public function getCoordinator(): ?User
    {
        return $this->coordinator;
    }

    public function setCoordinator(?User $coordinator): self
    {
        $this->coordinator = $coordinator;

        return $this;
    }

    public function getSyllabi(): ?string
    {
        return $this->syllabi;
    }

    public function setSyllabi(?string $syllabi): self
    {
        $this->syllabi = $syllabi;

        return $this;
    }

    public function getSyllabiStatus(): ?string
    {
        return $this->syllabiStatus;
    }

    public function setSyllabiStatus(string $syllabiStatus): self
    {
        $this->syllabiStatus = $syllabiStatus;

        return $this;
    }

    public function getArtifacts(): ?string
    {
        return $this->artifacts;
    }

    public function setArtifacts(?string $artifacts): self
    {
        $this->artifacts = $artifacts;

        return $this;
    }

    public function getArtifactsStatus(): ?string
    {
        return $this->artifactsStatus;
    }

    public function setArtifactsStatus(string $artifactsStatus): self
    {
        $this->artifactsStatus = $artifactsStatus;

        return $this;
    }

    public function getOutcomes(): ?string
    {
        return $this->outcomes;
    }

    public function setOutcomes(?string $outcomes): self
    {
        $this->outcomes = $outcomes;

        return $this;
    }

    public function getOutcomesStatus(): ?string
    {
        return $this->outcomesStatus;
    }

    public function setOutcomesStatus(string $outcomesStatus): self
    {
        $this->outcomesStatus = $outcomesStatus;

        return $this;
    }

    public function __toString() {
        return $this->getName()->getCode() . ' ' . $this->term . ' ' . $this->year;
    }

    /**
     * @return Collection|User[]
     */
    public function getTeachers(): Collection
    {
        return $this->teachers;
    }

    public function addTeacher(User $teacher): self
    {
        if (!$this->teachers->contains($teacher)) {
            $this->teachers[] = $teacher;
            $teacher->addCourse($this);
        }

        return $this;
    }

    public function removeTeacher(User $teacher): self
    {
        if ($this->teachers->contains($teacher)) {
            $this->teachers->removeElement($teacher);
            $teacher->removeCourse($this);
        }

        return $this;
    }
}
