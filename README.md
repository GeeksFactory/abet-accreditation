# ABET accreditation platform

This [project][ABET accreditation] is a platform that handles the process of the
ABET accreditation.

## Contribution

Thank you for your interest in our project. There are many ways to contribute,
and we appreciate all of them :

- Source code
- Unit tests
- Graphics
- Bug Reports
- Documentation
- Localization (Translation)

Contributions should be under the terms of both the MIT license
[&lt;LICENSE-MIT&gt;][LICENSE-MIT] and the Apache License (Vertion 2.0)
[&lt;LICENSE-APACHE&gt;][LICENSE-APACHE].

Documentation and graphics contributions should be under the terms
of the MIT license [&lt;LICENSE-MIT&gt;][LICENSE-MIT], the Apache License
(Version 2.0) [&lt;LICENSE-APACHE&gt;][LICENSE-APACHE] and the Creative Commons
Attribution 4.0 International License [&lt;DOC-LICENSE&gt;][DOC-LICENSE].

See [CONTRIBUTING.md][Contributing], for details.

## License

This project is primarily distributed under the terms of both the MIT license
and the Apache License (Version 2.0), with portions covered by various BSD-like
licenses. See [COPYRIGHT][COPYRIGHT], [LICENSE-APACHE][LICENSE-APACHE],
[LICENSE-MIT][LICENSE-MIT], [DOC-LICENSE][DOC-LICENSE], [AUTHORS][AUTHORS] and
[CONTRIBUTORS][CONTRIBUTORS] for more details.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



[AUTHORS]: AUTHORS
[Contributing]: CONTRIBUTING.md
[CONTRIBUTORS]: CONTRIBUTORS
[COPYRIGHT]: COPYRIGHT
[ABET accreditation]: https://gitlab.com/GeeksFactory/abet-accreditation
[DOC-LICENSE]: DOC-LICENSE
[LICENSE-APACHE]: LICENSE-APACHE
[LICENSE-MIT]: LICENSE-MIT
