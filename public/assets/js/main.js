$(function() {

    // Sidebar items selection
    $('.sidebar .nav-item .collapse-item').each(function(){
        if($(this).attr('href') == location.pathname) {
            $(this).addClass('active');
        }
    });

    // Initialize tooltips
    $('[data-toggle="tooltip"]:not([disabled])').tooltip();
});
