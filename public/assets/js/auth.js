/**
 * This file is part of the ABET accreditation platform:
 * https://gitlab.com/GeeksFactory/abet-accreditation
 *
 * Copyright (C) 2019 Marwen Dallel <marwen.dallel@medtech.tn>
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License")
 * <LICENSE-APACHE> or the MIT license <LICENSE-MIT>, at your option.
 * You may not use this file except in compliance with one of the two licenses.
 * You should have received a copy of both the Apache License, Version 2.0 and
 * the MIT license along with this project. If not, you may obtain a copy of
 * the Apache License, Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0
 * and you may obtain a copy of the MIT license at
 * https://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

$("select#registration_form_roles").change(function(){
	var selectedRole = $(this).children("option:selected").val();
	if (selectedRole == "student") {
		$("#hideClass").removeClass("d-none");
	} else {
		$("#hideClass").addClass("d-none");
	}
});

$(".toggle-password").click(function() {
	$(this).children().toggleClass("fa-eye fa-eye-slash");
	var input = $($(this).attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});
